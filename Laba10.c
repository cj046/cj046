include <stdio.h>
int main()
{
    int num1, num2, sum,subtract,product,quotient,remainder;
    int *ptr1, *ptr2;

    ptr1 = &num1; 
    ptr2 = &num2; 

    printf("Enter any two numbers:\n ");
    scanf("%d%d", ptr1, ptr2);

    sum = *ptr1 + *ptr2;
    subtract=*ptr1-*ptr2;
    product=(*ptr1)*(*ptr2);
    quotient=(*ptr1)/(*ptr2);
    remainder=*ptr1%*ptr2;
    printf("Sum =%d+%d=%d\n",*ptr1,*ptr2,sum);
    printf("Difference=%d-%d=%d\n",*ptr1,*ptr2,subtract);
    printf("Product=%d*%d=%d\n",*ptr1,*ptr2,product);
    printf("Quotient=%d/%d=%d\n",*ptr1,*ptr2,quotient);
    printf("Remainder=%d/%d=%d\n",*ptr1,*ptr2,remainder);
    return 0;
}