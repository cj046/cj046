//THE SEARCH USED FOR SORTED ARRAY IS BINARY SEARCH
#include<stdio.h>
int main()
{
    int a[6],f=0,x,i,lb=0,ub=5,p=0;
    printf("Enter the values of array \n");
    for(i=0;i<6;i++)
        scanf("%d",&a[i]);
    printf("Enter the element to be searched \n");
    scanf("%d",&x);
    while(lb<ub)
    {
        p=(lb+ub)/2;
        if(a[p]>x)
            ub=p-1;
        else if(a[p]<x)
            lb=p+1;
        else
        {
            f=1;
            break;
        }
    }
    if(f==1)
        printf("Search Successful. %d is present at position %d\n",x,p);
    else
        printf("Search Unsuccessful\n");
    return 0;
}