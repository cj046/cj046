Subtraction:
#include<stdio.h>
//to read two matrix and find difference of two matrix display the result
int main()
{
    int a[20][20],b[20][20],c[20][20],i,j,r1,c1,r2,c2;
    printf("MATRIX 1\n");
    printf("Enter the rows: ");
    scanf("%d",&r1);
    printf("Enter the columns: ");
    scanf("%d",&c1);
    printf("MATRIX 2\n");
    printf("Enter the rows: ");
    scanf("%d",&r2);
    printf("Enter the columns: ");
    scanf("%d",&c2);
    if((r1==r2)&&(c1==c2))
    {
        printf("MATRIX 1 ELEMENTS:\n");
        for(i=0;i<r1;i++)
        {
            for(j=0;j<c1;j++)
            {
                scanf("%d",&a[i][j]);
            }
        }
        printf("MATRIX 2 ELEMENTS:\n");
        for(i=0;i<r2;i++)
        {
            for(j=0;j<c2;j++)
            {
                scanf("%d",&b[i][j]);
            }
        }
        for(i=0;i<r2;i++)
        {
            for(j=0;j<c2;j++)
            {
                c[i][j]=a[i][j]-b[i][j];
            }
        }
        printf("Resultant matrix is:\n");
        for(i=0;i<r2;i++)
        {
            for(j=0;j<c2;j++)
            {
                printf("%d ",c[i][j]);
            }
            printf("\n");
        }
    }
    else
        printf("Subtraction cannot be perforned for the given values of rows and columns");
    return 0;
}

